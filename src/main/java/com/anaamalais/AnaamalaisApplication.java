package com.anaamalais;

import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnaamalaisApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnaamalaisApplication.class, args);
	}

}
