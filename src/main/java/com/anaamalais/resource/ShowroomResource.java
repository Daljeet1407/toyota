package com.anaamalais.resource;

import com.anaamalais.entity.Showroom;
import com.anaamalais.repository.ShowroomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/showrooms")
public class ShowroomResource {

    @Autowired
    private ShowroomRepository showroomRepository;

    @GetMapping("/{id}")
    public Optional<Showroom> getShowroomById(@PathVariable Long id) {
        return showroomRepository.findById(id);
    }

    @GetMapping
    public List<Showroom> getShowroomsByStateAndCity(@RequestParam String state, @RequestParam String city) {
        return (List<Showroom>) showroomRepository.findByStateAndCity(state, city);
    }

    @GetMapping("/topsales")
    public Showroom getShowroomWithHighestMonthlySales() {
        return showroomRepository.findTopByOrderByMonthlySalesCountDesc();
    }

    @PostMapping
    public Showroom createShowroom(@RequestBody Showroom showroom) {
        return showroomRepository.save(showroom);
    }

    @PutMapping("/{id}")
    public Showroom updateShowroom(@PathVariable Long id, @RequestBody Showroom showroom) {
        showroom.setShowroomId(id);
        return showroomRepository.save(showroom);
    }

    @DeleteMapping("/{id}")
    public void deleteShowroom(@PathVariable Long id) {
        showroomRepository.deleteById(id);
    }
}