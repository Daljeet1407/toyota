package com.anaamalais.repository;

import com.anaamalais.entity.Showroom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShowroomRepository extends JpaRepository<Showroom, Long> {
    Showroom findByShowroomId(Long showroomId);
    Showroom findByStateAndCity(String state, String city);
    Showroom findTopByOrderByMonthlySalesCountDesc();
}