package com.anaamalais.entity;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "showroom")
public class Showroom implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "showroom_id")
    private Long showroomId;

    @Column(name = "showroom_name")
    private String showroomName;

    @Column(name = "state")
    private String state;

    @Column(name = "city")
    private String city;

    @Column(name = "no_of_vehicles")
    private Integer noOfVehicles;

    @Column(name = "no_of_employees")
    private Integer noOfEmployees;

    @Column(name = "monthysales_count")
    private Integer monthlySalesCount;

	public void setShowroomId(Long id) {
		// TODO Auto-generated method stub
		
	}

    // Getters and Setters
}